#include <stdio.h>
#include <stdlib.h>

  void pedir_entero (int *var){

    printf ("Dime un numero entero: ");
    scanf ("%i", var); //Guarda el numero en el valor de var que es la direccion de numero.
}

  int main(){
  
  int numero;

    pedir_entero (&numero); //Llamamos a la función pedir_entero.
    printf ("El numero que has escrito es: %i\n", numero); //Imprime el valor de la varible numero.

  return 0;
}
