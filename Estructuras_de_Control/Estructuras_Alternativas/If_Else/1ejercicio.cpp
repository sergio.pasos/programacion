#include <stdio.h>
#include <stdlib.h>

    int main() {
    int op;
    printf("Indique un número: ");
    scanf("%i", &op);

    if(op % 2 == 0){ //Si el resultado de dividir entre dos tiene como resto 0, es par.
      printf("%i es par.\n", op);
    }   
      else{
      printf("%i es impar.\n", op);
    }
    
    return 0;
}
