#include <stdio.h>
#include <stdlib.h>

int main(){

int op1;
int op2;
int suma;

printf("Elige el primer operando " );
scanf("%i", &op1);

printf("Elige el segundo operando " );
scanf("%i", &op2);

// OPERACIÓN DE SUMAR
suma = op1 + op2;

printf("El resultado de sumar  \033[0;36m%i \033[0;37m mas \033[0;36m%i \033[0;37m es igual a \033[0;32m%i\n", op1, op2, suma);

return 0;
}
