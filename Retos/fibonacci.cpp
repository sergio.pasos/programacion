#include <stdio.h>
#include <stdlib.h>
#include <stdio_ext.h>

void preguntar_numeros (int *a, int *b) { //Función que pregunta los números para empezar la serie.

	printf ("Escoje un número: ");
	scanf ("%i", a);

	__fpurge (stdin);

	printf ("Escoje un número: ");
	scanf ("%i", b);
}

void preguntar_cont (int *cont) {

	printf ("Cuántas veces quieres que se repita la serie: "); //Función que pregunta la longitud de la serie.
	scanf ("%i", cont);
}

int main () {

	int num1;
	int num2;
	int cont;

	//Llamada a las funciones.
	preguntar_numeros (&num1, &num2);
	preguntar_cont (&cont);

	//Algoritmo	
	for (int i=0; i<cont; i++){ 
		int suma;
		suma = num1 + num2;
		printf ("%i\n", suma);
		num1 = num2;
		num2 = suma;
	}
}
