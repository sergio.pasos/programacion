#include <stdio.h>
#include <stdlib.h>

int main(){

    unsigned l;

    printf("Longitud tablero: ");
    scanf("%u", &l);

    for(int f=0; f<l; f++){
        for(int f2=0; f2<l; f2++){
            for(int c=0; c<l; c++){
                for(int c2=0; c2<l; c2++){
                    if((f+c)%2!=0)
                        printf("* ");
                    else
                        printf("- ");
                }
            }
        printf("\n");
        }
    }
    
    return EXIT_SUCCESS;
}
