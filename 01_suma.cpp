#include <stdio.h>
#include <stdlib.h>

int globl = 0xDEADBEEF;

int main() {

    	int op1, op2, op3, op4, result;

	op1 = 10;
	op2 = 100;
	op3 = 1000;
	op4 = 10000;
	result = op1 + op2 + op3 + op4;
			
	printf ("%i + %i + %i + %i = %i\n", op1, op2, op3, op4, result);

		
	return EXIT_SUCCESS; 

}	
